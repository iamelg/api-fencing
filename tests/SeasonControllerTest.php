<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SeasonControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    public $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testShowSeasons()
    {
        $crawler = $this->client->request('GET', 'api-fencing/seasons');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }

    public function testShowOneSeason()
    {
        $crawler = $this->client->request('GET', 'api-fencing/seasons/1');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }
}

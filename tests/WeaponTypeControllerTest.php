<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WeaponTypeControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    public $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testShowWepons()
    {
        $crawler = $this->client->request('GET', 'api-fencing/weapons');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }

    public function testShowOneWepon()
    {
        $crawler = $this->client->request('GET', 'api-fencing/weapons/1');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }
}
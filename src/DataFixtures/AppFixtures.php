<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Fencer;
use App\Entity\WeaponType;
use App\Entity\Team;
use App\Entity\Season;
use App\Entity\Event;
use App\services\UploadService;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;


class AppFixtures extends Fixture
{
    private $uploadService;
    private $filesystem;

    public function __construct(UploadService $uploadService, Filesystem $filesystem)
    {
        $this->uploadService = $uploadService;
        $this->filesystem = $filesystem;
    }
    public function load(ObjectManager $manager)
    {

        //Team1
        $france = new Team();
        $france->setName('France');
        // $this->filesystem->copy(__DIR__ . '/../../assets/images/flag-france.png', __DIR__ . '/../../assets/images/flag_france.png');
        // $flag = $this->uploadService->upload(new File(__DIR__ . '/../../assets/images/flag_france.png'));
        $france->setFlag('https://fie.org/img/flags/4x3/fr.svg');

        $manager->persist($france);

        $this->addReference('france', $france);

        //Team2
        $italie = new Team();
        $italie->setName('Italie');
        // $this->filesystem->copy(__DIR__ . '/../../assets/images/flag-italie.png', __DIR__ . '/../../assets/images/flag_italie.png');
        // $flag = $this->uploadService->upload(new File(__DIR__ . '/../../assets/images/flag_italie.png'));
        $italie->setFlag('https://fie.org/img/flags/4x3/it.svg');

        $manager->persist($italie);

        $this->addReference('italie', $italie);

        //Weapons
        $foil = new  WeaponType();
        $foil->setName('Fleuret');

        $manager->persist($foil);

        $this->addReference('foil', $foil);

        //epee
        $epee = new WeaponType();
        $epee->setName('Epée');

        $manager->persist($epee);

        $this->addReference('epee', $epee);

        // Thibus
        $thibusGP19 = new Event();
        $thibusGP19->setName("FIE Grand Prix Anaheim");
        $thibusGP19->setLocation("USA");
        $thibusGP19->setStart(new \DateTime('16-03-2019'));
        $thibusGP19->setEnd(new \DateTime('17-03-2019'));
        $thibusGP19->setPoints(12);
        $thibusGP19->setRank(9);

        $thibusGP19->addTeam($this->getReference('france'));
        $thibusGP19->addWeaponType($this->getReference('foil'));

        $manager->persist($thibusGP19);

        $this->addReference('thibusGP19', $thibusGP19);

        $thibusEvent = new Event();
        $thibusEvent->setName('bloup');
        $thibusEvent->setLocation('France');
        $thibusEvent->setPoints(4);
        $thibusEvent->setRank(10);

        $manager->persist($thibusEvent);

        $this->addReference('thibusEvent', $thibusEvent);



        // Grand Prix 2019
        $grandPrix19 = new Event();
        $grandPrix19->setName("FIE Grand Prix Anaheim");
        $grandPrix19->setLocation("USA");
        $grandPrix19->setStart(new \DateTime('16-03-2019'));
        $grandPrix19->setEnd(new \DateTime('17-03-2019'));

        $grandPrix19->addTeam($this->getReference('france'));
        $grandPrix19->addWeaponType($this->getReference('foil'));

        $manager->persist($grandPrix19);

        $this->addReference('grandPrix19', $grandPrix19);

        $test1 = new Event();
        $test1->setName("test1");
        $test1->setLocation("USA");
        $test1->setStart(new \DateTime('17-03-2019'));
        $test1->setEnd(new \DateTime('18-03-2019'));

        $test1->addTeam($this->getReference('france'));
        $test1->addWeaponType($this->getReference('foil'));

        $manager->persist($test1);

        $this->addReference('test1', $test1);

        $test2 = new Event();
        $test2->setName("test2");
        $test2->setLocation("USA");
        $test2->setStart(new \DateTime('18-03-2019'));
        $test2->setEnd(new \DateTime('19-03-2019'));

        $test2->addTeam($this->getReference('france'));
        $test2->addWeaponType($this->getReference('foil'));

        $manager->persist($test2);

        $this->addReference('test2', $test2);

        $test3 = new Event();
        $test3->setName("test3");
        $test3->setLocation("USA");
        $test3->setStart(new \DateTime('19-03-2019'));
        $test3->setEnd(new \DateTime('20-03-2019'));

        $test3->addTeam($this->getReference('france'));
        $test3->addWeaponType($this->getReference('foil'));

        $manager->persist($test3);

        $this->addReference('test3', $test3);


        // Seasons
        // France 2018-2019
        $franceFF1819 = new Season();
        $franceFF1819->setYears("2018/2019");
        $franceFF1819->setTotal(364);
        $franceFF1819->addEvent($this->getReference('grandPrix19'));
        $franceFF1819->addTeam($this->getReference('france'));

        $manager->persist($franceFF1819);

        $this->addReference('franceFF1819', $franceFF1819);


        /* Fencers seasons */
        /* Thibus seasons */
        $thibus1718 = new Season();
        $thibus1718->setYears("2017/2018");
        $thibus1718->setTotal(134);

        $manager->persist($thibus1718);

        $this->addReference('thibus1718', $thibus1718);

        $thibus1819 = new Season();
        $thibus1819->setYears("2018/2019");
        $thibus1819->setTotal(139);

        $thibus1819->addEvent($this->getReference('thibusGP19'));

        $manager->persist($thibus1819);

        $this->addReference('thibus1819', $thibus1819);


        //Fencer1
        $thibus = new Fencer();
        $thibus->setName('Thibus');
        $thibus->setSurname('Ysaora');
        $thibus->setAge(27);
        $thibus->setGender('F');
        $thibus->setHandness('D');
        $thibus->setStart(7);
        $thibus->setPhoto('https://static.fie.org/uploads/7/39187-THIBUS-Ysaora.jpg');
        $thibus->setTeam($this->getReference('france'));

        $thibus->addWeaponType($this->getReference('foil'));
        $thibus->addEvent($this->getReference('thibusGP19'));
        $thibus->addEvent($this->getReference('thibusEvent'));
        $thibus->addSeason($this->getReference('thibus1718'));
        $thibus->addSeason($this->getReference('thibus1819'));

        $manager->persist($thibus);

        $flessel = new Fencer();
        $flessel->setName('Flessel');
        $flessel->setSurname('Laura');
        $flessel->setAge(47);
        $flessel->setGender('F');
        $flessel->setHandness('G');
        $flessel->setStart(6);
        $flessel->setEnd(40);
        $flessel->setPhoto('http://www.123savoie.com/pic/33/32364_t6.jpg');
        $flessel->setTeam($this->getReference('france'));

        $flessel->addWeaponType($this->getReference('epee'));

        $manager->persist($flessel);

        $lePechoux = new Fencer();
        $lePechoux->setName('Le Pechoux');
        $lePechoux->setSurname('Erwann');
        $lePechoux->setAge(27);
        $lePechoux->setGender('M');
        $lePechoux->setHandness('G');
        $lePechoux->setStart(7);
        $lePechoux->setPhoto('https://static.fie.org/uploads/9/49058-38.jpg');
        $lePechoux->setTeam($this->getReference('france'));

        $lePechoux->addWeaponType($this->getReference('foil'));

        $manager->persist($lePechoux);

        $manager->flush();
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FencerRepository")
 */
class Fencer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="integer")
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $handness;

    /**
     * @ORM\Column(type="integer")
     */
    private $start;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $end;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\WeaponType", mappedBy="fencers")
     */
    private $weaponTypes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gender;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Team", inversedBy="fencers")
     */
    private $team;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Season", inversedBy="fencers")
     */
    private $seasons;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Event", mappedBy="fencers")
     */
    private $events;

    public function __construct()
    {
        $this->weaponTypes = new ArrayCollection();
        $this->seasons = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getHandness(): ?string
    {
        return $this->handness;
    }

    public function setHandness(string $handness): self
    {
        $this->handness = $handness;

        return $this;
    }

    public function getStart(): ?int
    {
        return $this->start;
    }

    public function setStart(int $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?int
    {
        return $this->end;
    }

    public function setEnd(?int $end): self
    {
        $this->end = $end;

        return $this;
    }

    /**
     * @return Collection|WeaponType[]
     */
    public function getWeaponTypes(): Collection
    {
        return $this->weaponTypes;
    }

    public function addWeaponType(WeaponType $weaponType): self
    {
        if (!$this->weaponTypes->contains($weaponType)) {
            $this->weaponTypes[] = $weaponType;
            $weaponType->addFencer($this);
        }

        return $this;
    }

    public function removeWeaponType(WeaponType $weaponType): self
    {
        if ($this->weaponTypes->contains($weaponType)) {
            $this->weaponTypes->removeElement($weaponType);
            $weaponType->removeFencer($this);
        }

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    /**
     * @return Collection|Season[]
     */
    public function getSeasons(): Collection
    {
        return $this->seasons;
    }

    public function addSeason(Season $season): self
    {
        if (!$this->seasons->contains($season)) {
            $this->seasons[] = $season;
        }

        return $this;
    }

    public function removeSeason(Season $season): self
    {
        if ($this->seasons->contains($season)) {
            $this->seasons->removeElement($season);
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addFencer($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            $event->removeFencer($this);
        }

        return $this;
    }
}

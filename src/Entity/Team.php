<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeamRepository")
 */
class Team
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $flag;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Fencer", mappedBy="team")
     */
    private $fencers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Event", mappedBy="teams")
     */
    private $events;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Season", mappedBy="teams")
     */
    private $seasons;

    public function __construct()
    {
        $this->fencers = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->seasons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFlag(): ?string
    {
        return $this->flag;
    }

    public function setFlag(?string $flag): self
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * @return Collection|Fencer[]
     */
    public function getFencers(): Collection
    {
        return $this->fencers;
    }

    public function addFencer(Fencer $fencer): self
    {
        if (!$this->fencers->contains($fencer)) {
            $this->fencers[] = $fencer;
            $fencer->setTeam($this);
        }

        return $this;
    }

    public function removeFencer(Fencer $fencer): self
    {
        if ($this->fencers->contains($fencer)) {
            $this->fencers->removeElement($fencer);
            // set the owning side to null (unless already changed)
            if ($fencer->getTeam() === $this) {
                $fencer->setTeam(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addTeam($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            $event->removeTeam($this);
        }

        return $this;
    }

    /**
     * @return Collection|Season[]
     */
    public function getSeasons(): Collection
    {
        return $this->seasons;
    }

    public function addSeason(Season $season): self
    {
        if (!$this->seasons->contains($season)) {
            $this->seasons[] = $season;
            $season->addTeam($this);
        }

        return $this;
    }

    public function removeSeason(Season $season): self
    {
        if ($this->seasons->contains($season)) {
            $this->seasons->removeElement($season);
            $season->removeTeam($this);
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WeaponTypeRepository")
 */
class WeaponType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Fencer", inversedBy="weaponTypes")
     */
    private $fencers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Event", mappedBy="weaponTypes")
     */
    private $events;

    public function __construct()
    {
        $this->fencers = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Fencer[]
     */
    public function getFencers(): Collection
    {
        return $this->fencers;
    }

    public function addFencer(Fencer $fencer): self
    {
        if (!$this->fencers->contains($fencer)) {
            $this->fencers[] = $fencer;
        }

        return $this;
    }

    public function removeFencer(Fencer $fencer): self
    {
        if ($this->fencers->contains($fencer)) {
            $this->fencers->removeElement($fencer);
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->addWeaponType($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            $event->removeWeaponType($this);
        }

        return $this;
    }
}

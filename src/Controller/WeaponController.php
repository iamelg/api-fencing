<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use App\Repository\WeaponTypeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\WeaponType;

/**
 * @Route("api-fencing/weapons")
 */
class WeaponController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    public function __construct(SerializerInterface $serializer)
    {
       $this->serializer = $serializer; 
    }

    /**
     * @Route(methods="GET")
     */
    public function getAllWeapons(WeaponTypeRepository $weaponRepo)
    {
        $weapon = $weaponRepo->findAll();
        $json = $this->serializer->serialize($weapon, 'json');
        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/{weapon}", methods="GET")
     */
    public function getOneWeapon(WeaponType $weapon)
    {
        return new JsonResponse($this->serializer->serialize($weapon, 'json'), 200, [], true);
    }
}
